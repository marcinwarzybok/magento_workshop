<?php

class Pgs_Workshop_Model_Resource_Subscription extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('pgs_workshop/subscription', 'subscription_id');
    }
}