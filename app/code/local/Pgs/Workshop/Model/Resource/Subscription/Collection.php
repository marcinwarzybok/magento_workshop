<?php

class Pgs_Workshop_Model_Resource_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('pgs_workshop/subscription');
    }
}