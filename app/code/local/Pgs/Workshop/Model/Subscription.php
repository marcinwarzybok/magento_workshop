<?php

class Pgs_Workshop_Model_Subscription extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('pgs_workshop/subscription');
    }
}