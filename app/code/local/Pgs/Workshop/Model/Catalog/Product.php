<?php

class Pgs_Workshop_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    public function getName()
    {
        return 'PGS - ' . $this->getData('name');
    }
}