<?php

class Pgs_Workshop_Block_FirstBlock extends Mage_Core_Block_Template
{
    public function getProducts()
    {
        $products = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
//            ->addAttributeToFilter('price', array(
//                'in' => array(175, 180, 190)
//            ))
//            ->addAttributeToFilter('price', array( 'gt' => 180 ) )
//            ->addAttributeToFilter('price', array( 'lt' => 500 ) )
//            ->addAttributeToFilter('name', array(
//                'like' => '%Shirt%'
//            ))
            ->setPageSize(5);

        return $products;
    }
}